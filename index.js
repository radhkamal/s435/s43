let posts = [];
let count = 1;

// Add post data
document.querySelector('#form-addPost').addEventListener('submit', (e) => {

	// prevents the page from loading
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#text-title').value,
		body: document.querySelector('#text-body').value
	});

	count++;

	showPosts(posts);
	alert('successfully Added!');
});

const showPosts = (posts) => {

	let postEntries = '';

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3	id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	document.querySelector('#postEntries').innerHTML = postEntries;
}

// Editing a Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#text-editId').value = id;
	document.querySelector('#text-editTitle').value = title;
	document.querySelector('#text-editBody').value = body;
}

// Update Post
document.querySelector('#form-editPost').addEventListener('submit', (e) => {

	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector('#text-editId').value) {

			console.log(posts[i].id);
			posts[i].title = document.querySelector('#text-editTitle').value;
			posts[i].body = document.querySelector('#text-editBody').value;

			showPosts(posts);
			alert('Successfully updated!');

			break;
		}
	}
})



const deletePost = (id) => {

	for (let i = 0; i < posts.length; i++) {

		if(posts[i].id == id) {
			posts.splice(i, 1);
		}
	}
	
	showPosts(posts);
}
